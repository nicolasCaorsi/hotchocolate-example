## Installation

```bash
  dotnet restore
```

## Run Locally

```bash
  dotnet run --project GraphQL
```
